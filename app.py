from flask import Flask, render_template, request
import sqlite3 as sql
import tensorflow_hub as hub
import pandas as pd
import tensorflow as tf

#Load Embedding models
embed = hub.load("https://tfhub.dev/google/nnlm-en-dim50/2")

app = Flask(__name__)

def flag_similarity(string1):
  
   # Read sqlite query results into a pandas DataFrame
   thresh_cosine = -0.1
   index_similar = None
   con = sql.connect("database.db")
   df = pd.read_sql_query("SELECT * from qna", con)

   for row in range(0, len(df)): 
      embeddings = embed([string1,df.loc[row,'question']])
      quest_cosine = tf.keras.losses.cosine_similarity(embeddings[0], embeddings[1], axis=-1)
      quest_cosine = quest_cosine.numpy()

      if quest_cosine <= thresh_cosine:
         thresh_cosine = quest_cosine
         index_similar = row
   if index_similar != None:
      output = df.loc[index_similar,'answer']
   else:
      output = "Sorry, We don't have the answer"
      
   return output


@app.route("/")
def home():
    return render_template("home.html")

@app.route("/get")
def get_bot_response():
    userText = request.args.get('msg')
    answer = flag_similarity(userText)
    return str(answer)

@app.route('/enternew')
def new_qna():
   return render_template('qna.html')

@app.route('/addrec',methods = ['POST', 'GET'])
def addrec():
   if request.method == 'POST':
      try:
         question = request.form['question']
         answer = request.form['answer']
         
         with sql.connect("database.db") as con:
            cur = con.cursor()
            
            cur.execute("INSERT INTO qna (question,answer) VALUES (?,?)",(question,answer) )
            
            con.commit()
            msg = "Record successfully added"
      except:
         con.rollback()
         msg = "error in insert operation"
      
      finally:
         return render_template("result.html",msg = msg)
         con.close()

@app.route('/list')
def list():
   con = sql.connect("database.db")
   con.row_factory = sql.Row
   
   cur = con.cursor()
   cur.execute("select * from qna")
   
   rows = cur.fetchall();
   return render_template("list.html",rows = rows)
if __name__ == "__main__":
    app.run(debug=True)